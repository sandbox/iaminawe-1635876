<?php
/**
 * @file
 * Views Slideshow: Single Frame template file.
 *
 * - $rows: The themed rows from the view.
 * - $settings: The settings for the views slideshow type.
 * - $view: The view object.
 * - $vss_id: The views slideshow id.
 */
?>


<div id="da-slider" class="da-slider">


    <?php foreach ($rows as $row): ?>
  	<div class="da-slide">
   
<?php print $row; ?>

	</div>
    <?php endforeach; ?>

				<nav class="da-arrows">
					<span class="da-arrows-prev"></span>
					<span class="da-arrows-next"></span>
				</nav>
</div>
<script type="text/javascript">
(function ($) {
        $(document).ready(function() {     
       $('#da-slider').cslider({
				});
    });})(jQuery)
		</script>	