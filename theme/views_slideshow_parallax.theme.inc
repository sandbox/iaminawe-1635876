<?php
/**
 * @file views_slideshow_parallax.theme.inc
 */

/**
 * Implements hook_preprocess_HOOK().
 */
function views_slideshow_parallax_preprocess_views_slideshow_parallax_main_frame(&$variables) {
  $module_path = drupal_get_path('module', 'views_slideshow_parallax');

  $settings = $variables['settings'];
  $vss_id = $variables['vss_id'];
  $settings['vss_id'] = $vss_id;

  //drupal_add_css($module_path . '/css/animate.min.css');
  drupal_add_css($module_path . '/css/parallax_slideshow.css');
  //drupal_add_css($module_path . '/css/nojs.css');
  drupal_add_js($module_path . '/js/jquery.cslider.js');
  drupal_add_js($module_path . '/js/modernizr.custom.28468.js');

}
