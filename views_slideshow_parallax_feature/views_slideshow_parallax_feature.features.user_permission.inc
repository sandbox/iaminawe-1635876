<?php
/**
 * @file
 * views_parallax_slider.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function views_parallax_slider_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create carousel content'.
  $permissions['create carousel content'] = array(
    'name' => 'create carousel content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any carousel content'.
  $permissions['delete any carousel content'] = array(
    'name' => 'delete any carousel content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own carousel content'.
  $permissions['delete own carousel content'] = array(
    'name' => 'delete own carousel content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any carousel content'.
  $permissions['edit any carousel content'] = array(
    'name' => 'edit any carousel content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own carousel content'.
  $permissions['edit own carousel content'] = array(
    'name' => 'edit own carousel content',
    'roles' => array(),
    'module' => 'node',
  );

  return $permissions;
}
