<?php
/**
 * @file
 * views_slideshow_parallax_feature.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function views_slideshow_parallax_feature_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'home_carousel';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Home Carousel';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = '<none>';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '7';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'slideshow';
  $handler->display->display_options['style_options']['slideshow_type'] = 'views_slideshow_parallax';
  $handler->display->display_options['style_options']['slideshow_skin'] = 'default';
  $handler->display->display_options['style_options']['skin_info'] = array(
    'class' => 'default',
    'name' => 'Default',
    'module' => 'views_slideshow_parallax',
    'path' => '',
    'stylesheets' => array(),
  );
  $handler->display->display_options['style_options']['widgets']['top']['views_slideshow_pager']['weight'] = '1';
  $handler->display->display_options['style_options']['widgets']['top']['views_slideshow_pager']['type'] = 'views_slideshow_pager_fields';
  $handler->display->display_options['style_options']['widgets']['top']['views_slideshow_pager']['views_slideshow_pager_fields_fields'] = array(
    'field_carousel_logo' => 0,
    'title' => 0,
    'field_carousel_image' => 0,
    'body' => 0,
    'field_carousel_bg_image' => 0,
  );
  $handler->display->display_options['style_options']['widgets']['top']['views_slideshow_controls']['weight'] = '1';
  $handler->display->display_options['style_options']['widgets']['top']['views_slideshow_controls']['type'] = 'views_slideshow_controls_text';
  $handler->display->display_options['style_options']['widgets']['top']['views_slideshow_slide_counter']['weight'] = '1';
  $handler->display->display_options['style_options']['widgets']['bottom']['views_slideshow_pager']['weight'] = '1';
  $handler->display->display_options['style_options']['widgets']['bottom']['views_slideshow_pager']['type'] = 'views_slideshow_pager_fields';
  $handler->display->display_options['style_options']['widgets']['bottom']['views_slideshow_pager']['views_slideshow_pager_fields_fields'] = array(
    'field_carousel_logo' => 0,
    'title' => 0,
    'field_carousel_image' => 0,
    'body' => 0,
    'field_carousel_bg_image' => 0,
  );
  $handler->display->display_options['style_options']['widgets']['bottom']['views_slideshow_controls']['weight'] = '1';
  $handler->display->display_options['style_options']['widgets']['bottom']['views_slideshow_controls']['type'] = 'views_slideshow_controls_text';
  $handler->display->display_options['style_options']['widgets']['bottom']['views_slideshow_slide_counter']['weight'] = '1';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Footer: Global: Text area */
  $handler->display->display_options['footer']['area']['id'] = 'area';
  $handler->display->display_options['footer']['area']['table'] = 'views';
  $handler->display->display_options['footer']['area']['field'] = 'area';
  $handler->display->display_options['footer']['area']['format'] = 'ds_code';
  /* Field: Content: Link */
  $handler->display->display_options['fields']['field_carousel_link']['id'] = 'field_carousel_link';
  $handler->display->display_options['fields']['field_carousel_link']['table'] = 'field_data_field_carousel_link';
  $handler->display->display_options['fields']['field_carousel_link']['field'] = 'field_carousel_link';
  $handler->display->display_options['fields']['field_carousel_link']['label'] = '';
  $handler->display->display_options['fields']['field_carousel_link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_carousel_link']['element_type'] = 'div';
  $handler->display->display_options['fields']['field_carousel_link']['element_class'] = 'da-link';
  $handler->display->display_options['fields']['field_carousel_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_carousel_link']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_carousel_link']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_carousel_link']['type'] = 'link_plain';
  /* Field: Content: Logo */
  $handler->display->display_options['fields']['field_carousel_logo']['id'] = 'field_carousel_logo';
  $handler->display->display_options['fields']['field_carousel_logo']['table'] = 'field_data_field_carousel_logo';
  $handler->display->display_options['fields']['field_carousel_logo']['field'] = 'field_carousel_logo';
  $handler->display->display_options['fields']['field_carousel_logo']['label'] = '';
  $handler->display->display_options['fields']['field_carousel_logo']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_carousel_logo']['alter']['path'] = '[field_carousel_link]';
  $handler->display->display_options['fields']['field_carousel_logo']['alter']['absolute'] = TRUE;
  $handler->display->display_options['fields']['field_carousel_logo']['element_type'] = 'div';
  $handler->display->display_options['fields']['field_carousel_logo']['element_class'] = 'da-logo';
  $handler->display->display_options['fields']['field_carousel_logo']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_carousel_logo']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_carousel_logo']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_carousel_logo']['settings'] = array(
    'image_style' => 'carousel_logo',
    'image_link' => '',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h2';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_carousel_image']['id'] = 'field_carousel_image';
  $handler->display->display_options['fields']['field_carousel_image']['table'] = 'field_data_field_carousel_image';
  $handler->display->display_options['fields']['field_carousel_image']['field'] = 'field_carousel_image';
  $handler->display->display_options['fields']['field_carousel_image']['label'] = '';
  $handler->display->display_options['fields']['field_carousel_image']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_carousel_image']['alter']['path'] = '[field_carousel_link]';
  $handler->display->display_options['fields']['field_carousel_image']['alter']['absolute'] = TRUE;
  $handler->display->display_options['fields']['field_carousel_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_carousel_image']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['field_carousel_image']['element_wrapper_class'] = 'da-img stack twisted';
  $handler->display->display_options['fields']['field_carousel_image']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_carousel_image']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_carousel_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_carousel_image']['settings'] = array(
    'image_style' => 'carousel_image',
    'image_link' => '',
  );
  /* Field: Content: Caption */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['exclude'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['path'] = '[field_carousel_link]';
  $handler->display->display_options['fields']['body']['element_type'] = 'p';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['body']['type'] = 'text_plain';
  /* Field: Content: Background Image */
  $handler->display->display_options['fields']['field_carousel_bg_image']['id'] = 'field_carousel_bg_image';
  $handler->display->display_options['fields']['field_carousel_bg_image']['table'] = 'field_data_field_carousel_bg_image';
  $handler->display->display_options['fields']['field_carousel_bg_image']['field'] = 'field_carousel_bg_image';
  $handler->display->display_options['fields']['field_carousel_bg_image']['label'] = '';
  $handler->display->display_options['fields']['field_carousel_bg_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_carousel_bg_image']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['field_carousel_bg_image']['element_wrapper_class'] = 'da-bg';
  $handler->display->display_options['fields']['field_carousel_bg_image']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_carousel_bg_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_carousel_bg_image']['settings'] = array(
    'image_style' => 'carousel_bg',
    'image_link' => '',
  );
  /* Sort criterion: Global: Random */
  $handler->display->display_options['sorts']['random']['id'] = 'random';
  $handler->display->display_options['sorts']['random']['table'] = 'views';
  $handler->display->display_options['sorts']['random']['field'] = 'random';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'carousel' => 'carousel',
  );
  /* Filter criterion: Content: Promoted to front page */
  $handler->display->display_options['filters']['promote']['id'] = 'promote';
  $handler->display->display_options['filters']['promote']['table'] = 'node';
  $handler->display->display_options['filters']['promote']['field'] = 'promote';
  $handler->display->display_options['filters']['promote']['value'] = '1';

  /* Display: HomeCarousel */
  $handler = $view->new_display('block', 'HomeCarousel', 'block');
  $export['home_carousel'] = $view;

  return $export;
}
