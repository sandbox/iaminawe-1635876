<?php
/**
 * @file
 * views_slideshow_parallax_feature.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function views_slideshow_parallax_feature_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'parallax_slider';
  $context->description = 'Adds the parallax slider block to the home page';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-home_carousel-block' => array(
          'module' => 'views',
          'delta' => 'home_carousel-block',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Adds the parallax slider block to the home page');
  $export['parallax_slider'] = $context;

  return $export;
}
