-- SUMMARY --

Integrates jQuery Parallax Carousel features into views slideshow version 7.x.

-- REQUIREMENTS --
* Views Slideshow 7.x-3.x


-- INSTALLATION --

* Install as usual, see (http://drupal.org/documentation/install/modules-themes/modules-7)
  for further information.


-- CONTACT --

Current maintainer:
* Gregg Coppen (iaminawe)

-- CREDITS --

* This project was based on the Jquery Liquid Carousel developed by Jonathan DeLaigle (grndlvl) - http://drupal.org/user/103553 sponsored by Advomatic (http://www.advomatic.com).
* It makes use of the techniques shown in this tutorial at http://tympanus.net/codrops/2012/03/15/parallax-content-slider-with-css3-and-jquery/
